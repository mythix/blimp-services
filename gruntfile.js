module.exports = function (grunt) {
  'use strict';

  // Project configuration.
  grunt.initConfig({

    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*!\n' +
            ' * Blimp v<%= pkg.version %> (<%= pkg.homepage %>)\n' +
            ' * Copyright 2014-<%= grunt.template.today("yyyy") %>\n' +
            ' * Licensed under <%= pkg.license.type %> (<%= pkg.license.url %>)\n' +
            ' */\n',

    // Task configuration.
    clean: {
      dist: ['public/components']
    },

    jshint: {
      options: {
        reporter: require('jshint-stylish'),
        jshintrc: 'app/.jshintrc'
      },
      src: {
        src: ['app/**/*.js', 'utils/**/*.js', 'gruntfile.js']
      },
      assets: {
        // later to be used for public javascript
        src: ['public/**/*.js', '!public/components/**/*.js']
      },
      tests: {
        src: ['tests/**/*.model.test.js', 'tests/**/*.routes.test.js', '!tests/**/_*.js']
      }
    },

    // jscs: {
    //   options: {
    //     config: 'app/.jscsrc'
    //   },
    //   grunt: {
    //     src: '<%= jshint.grunt.src %>'
    //   },
    //   src: {
    //     src: '<%= jshint.src.src %>'
    //   },
    //   test: {
    //     src: '<%= jshint.tests.src %>'
    //   },
    //   assets: {
    //     options: {
    //       requireCamelCaseOrUpperCaseIdentifiers: null
    //     },
    //     src: '<%= jshint.assets.src %>'
    //   }
    // },

    mochaTest: {
      options: {
        reporter: 'spec',
        require: [
          'app.js'
        ]
      },
      src: ['<%= jshint.tests.src %>']
    },

    env: {
      test: {
        NODE_ENV: 'test'
      }
    },

    copy: {
      bower: {
        cwd: 'bower_components/bootstrap/dist/',
        expand: true,
        src: '**',
        dest: 'public/components/bootstrap/'
      },
      jquery: {
        cwd: 'bower_components/jquery/dist/',
        expand: true,
        src: '**',
        dest: 'public/components/jquery'
      }
    },

    watch: {
      src: {
        files: '<%= jshint.src.src %>',
        tasks: ['test', 'jshint:src']
      },
      assets: {
        files: '<%= jshint.assets.src %>',
        tasks: ['jshint:assets']
      },
      tests: {
        files: '<%= jshint.tests.src %>',
        tasks: ['test', 'jshint:tests']
      }
    }
  });


  // These plugins provide necessary tasks.
  require('load-grunt-tasks')(grunt, { /*scope: 'devDependencies'*/ });

  // Full distribution task.
  grunt.registerTask('dist', ['clean', 'copy']);

  // Full testing task
  grunt.registerTask('test', ['env:test', 'mochaTest']);

  // Default task.
  grunt.registerTask('default', ['dist', 'jshint', 'watch']);

};
