'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    request = require('supertest'),
    mongoose = require('mongoose'),
    _ = require('lodash'),
    config = require('./../app/config/config'),
    helper = require('./../utils/test-helper'),
    app = require('./../app/app'),
    Project = mongoose.model('Project'),
    Organisation = mongoose.model('Organisation');

/**
 * Globals
 */
var project1,
    organisation1,
    props = Object.keys(Project.schema.paths);

props = _.filter(props, function (prop) {
    return prop[0] !== '_';
});

/**
 * Test Suites
 */
describe('<Integration Test> Project routes', function() {

    // create an organisation to use in our projects
    before(function(done) {
        organisation1 = new Organisation({
            name: 'org_' + helper.getRandomString()
        });

        mongoose.connection.collections.projects.drop(function() {
            mongoose.connection.collections.project_versions.drop(function() {
                done();
            });
        });
    });

    describe('Start', function() {
        beforeEach(function(done) {
            project1 = {
                name: 'project_' + helper.getRandomString(),
                organisation: organisation1._id + ''
            };
            done();
        });

        it('should begin without the testing project', function(done) {
            // find project one to test if it really isn't there
            Project.find({
                name: project1.name
            }, function(err, projects) {
                projects.should.have.length(0);
                done();
            });
        });
    });

    describe('Schema', function () {

        it('should return the project\'s schema', function (done) {
            request(app)
                .get('/api/v1/projects/schema')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');
                    res.body.should.have.property('resource', 'Project');
                    res.body.should.have.property('fields');
                    res.body.fields.should.have.property('_id');
                    _.each(props, function (prop) {
                        res.body.fields.should.have.property(prop);
                    });
                });
            done();
        });
    });

    describe('Create', function() {
        beforeEach(function(done) {
            project1 = {
                name: 'project_' + helper.getRandomString(),
                organisation: organisation1._id + ''
            };
            done();
        });

        it('should add a new project', function(done) {
            // send creation request to the server
            request(app)
                .post('/api/v1/projects/')
                .send(project1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify result
                    res.should.have.property('status', 201);
                    res.should.have.property('body');
                    res.body.should.be.type('object');
                    res.body.name.should.equal(project1.name);

                    // verify database
                    Project.find({
                        _id: res.body._id
                    }, function(err, projects) {
                        projects.should.have.length(1);
                        done();
                    });
                });
        });

        it('should fail creating a project that already exists', function (done) {
            // create a project to try and double
            var _project = new Project(project1);
            _project.save(function(err) {
                should.not.exist(err);

                // send creation request to the server
                request(app)
                    .post('/api/v1/projects/')
                    .send(project1)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        res.should.have.property('status', 400);
                        res.should.have.property('body');
                        res.body.should.have.property('name', 'ValidationError');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property('name');
                        res.body.errors.name.should.have.property('path', 'name');

                        done();
                    });
            });
        });

        it('should fail creating a project without a name', function (done) {
            project1.name = undefined;

            // send creation request to the server
            request(app)
                .post('/api/v1/projects/')
                .send(project1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('name');
                    res.body.errors.name.should.have.property('path', 'name');

                    done();
                });
        });

        it('should fail creating a project without an organisation', function (done) {
            project1.organisation = undefined;

            // send creation request to the server
            request(app)
                .post('/api/v1/projects/')
                .send(project1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('organisation');
                    res.body.errors.organisation.should.have.property('path', 'organisation');

                    done();
                });
        });
    });

    describe('Read', function() {
        beforeEach(function(done) {
            project1 = {
                name: 'project_' + helper.getRandomString(),
                organisation: organisation1._id + ''
            };
            done();
        });

        it('should get projects', function(done) {
            // fetch projects
            request(app)
                .get('/api/v1/projects/')
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');

                    done();
            });
        });

        it('should get a specific project', function (done) {
            // create a project to fetch
            var _project = new Project(project1);
            _project.save(function(err) {
                should.not.exist(err);

                // fetch the project
                request(app)
                    .get('/api/v1/projects/' + _project._id)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body._id.should.equal(_project._id + '');

                        done();
                    });
            });
        });
    });

    describe('Update', function() {
        beforeEach(function(done) {
            project1 = {
                name: 'project_' + helper.getRandomString(),
                organisation: organisation1._id + ''
            };
            done();
        });

        it('should correctly update an existing project', function (done) {
            // create a project to update
            var _project = new Project(project1);
            _project.save(function(err) {
                should.not.exist(err);

                // update a field
                project1.description = 'Shiny description for our project';

                // send the update to the server
                request(app)
                    .put('/api/v1/projects/' + _project._id)
                    .send({
                        description: project1.description
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body.should.have.property('_id', _project._id + '');
                        res.body.should.have.property('description', project1.description);

                        // verify the database
                        Project.find({
                            _id: _project._id
                        }, function(err, projects) {
                            projects.should.have.length(1);
                            projects[0].should.have.property('description', project1.description);
                            done();
                        });
                    });
            });
        });
    });

    describe('Delete', function() {
        beforeEach(function(done) {
            project1 = {
                name: 'project_' + helper.getRandomString(),
                organisation: organisation1._id + ''
            };
            done();
        });

        it('should remove an existing project without problem', function (done) {
            // create a project to delete
            var _project = new Project(project1);
            _project.save(function(err) {
                should.not.exist(err);

                // send deletion request to the server
                request(app)
                    .delete('/api/v1/projects/' + _project._id)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify result
                        res.should.have.property('status', 204);

                        // verify database
                        Project.findById(_project._id, function(err, proj) {
                            should.not.exist(err);
                            should.exist(proj, 'Project doesn\'t exist');
                            proj.should.have.property('isDeleted', true);
                            done();
                        });
                    });
            });
        });
    });

    describe('Project History', function () {
        var _project;

        before(function (done) {
            project1 = {
                name: 'project_' + helper.getRandomString(),
                organisation: organisation1._id + ''
            };

            _project = new Project(project1);
            _project.save(function(err) {
                done();
            });
        });

        it('should be empty for a new project', function (done) {
            request(app)
                .get('/api/v1/projects/' + _project._id + '/history')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body', []);
                    done();
                });
        });

        it('should be have a change for each modification to the project', function (done) {
            var old = JSON.parse(JSON.stringify(project1));
            _project.set({
                name: 'newName'
            });

            _project.save(function(err, org) {
                request(app)
                    .get('/api/v1/projects/' + _project._id + '/history?populate=_refId')
                    .end(function(err, res) {
                        if(err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body');
                        res.body.length.should.equal(1);
                        _.each(props, function (prop) {
                            if(old[prop]) {
                                res.body[0].should.have.property(prop, old[prop]);
                            }
                        });
                        res.body[0].should.have.property('_action', 'save');
                        done();
                    });
            });
        });
    });
});
