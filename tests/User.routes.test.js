'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    request = require('supertest'),
    _ = require('lodash'),
    mongoose = require('mongoose'),
    config = require('./../app/config/config'),
    helper = require('./../utils/test-helper'),
    app = require('./../app/app'),
    User = mongoose.model('User'),
    Organisation = mongoose.model('Organisation');

/**
 * Globals
 */
var user1,
    organisation1,
    organisation2,
    props = Object.keys(User.schema.paths);

props = _.filter(props, function (prop) {
    return prop[0] !== '_';
});

/**
 * Test Suites
 */
describe('<Integration Test> User routes', function() {

    // create an organisation to use in our user tests
    before(function(done) {
        organisation1 = new Organisation({
            name: 'org_' + helper.getRandomString()
        });
        organisation2 = new Organisation({
            name: 'org_' + helper.getRandomString()
        });

        mongoose.connection.collections.users.drop(function() {
            mongoose.connection.collections.user_versions.drop(function() {
                done();
            });
        });
    });

    describe('Start', function() {
        before(function(done) {
            user1 = {
                firstName: 'first_' + helper.getRandomString(),
                lastName: 'last_' + helper.getRandomString(),
                email: 'test' + helper.getRandomString() + '@test.com',
                organisation: [organisation1._id + ''],
                gender: 'male'
            };

            done();
        });

        it('should begin without the testing user', function(done) {
            // find user one to test if it really isn't there
            User.find({
                email: user1.email
            }, function(err, users) {
                users.should.have.length(0);
                done();
            });
        });
    });

    describe('Schema', function () {

        it('should return the user\'s schema', function (done) {
            request(app)
                .get('/api/v1/users/schema')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');
                    res.body.should.have.property('resource', 'User');
                    res.body.should.have.property('fields');
                    res.body.fields.should.have.property('_id');
                    _.each(props, function (prop) {
                        res.body.fields.should.have.property(prop);
                    });
                });
            done();
        });
    });

    describe('Create', function() {

        beforeEach(function(done) {
            user1 = {
                firstName: 'first_' + helper.getRandomString(),
                lastName: 'last_' + helper.getRandomString(),
                email: 'test' + helper.getRandomString() + '@test.com',
                organisation: [organisation1._id + ''],
                gender: 'male'
            };

            mongoose.connection.collections.users.drop( function(err) {
                done();
            });

        });

        after(function (done) {
            mongoose.connection.collections.users.drop(function() {
                done();
            });
        });

        it('should add a new user', function(done) {
            // send creation request to the server
            request(app)
                .post('/api/v1/users/')
                .send(user1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify result
                    res.should.have.property('status', 201);
                    res.should.have.property('body');
                    res.body.should.be.type('object');
                    res.body.firstName.should.equal(user1.firstName);
                    res.body.lastName.should.equal(user1.lastName);
                    res.body.email.should.equal(user1.email);
                    res.body.gender.should.equal(user1.gender);
                    res.body.organisation.length.should.equal(1);
                    res.body.organisation[0].should.equal(user1.organisation[0]);

                    // verify database
                    User.find({
                        _id: res.body._id
                    }, function(err, users) {
                        users.should.have.length(1);

                        users[0].remove();
                        done();
                    });
                });
        });

        it('should fail creating a user that already exists', function (done) {
            var _user = new User(user1);
            _user.save(function(err) {
                should.not.exist(err);

                var cln = _.clone(user1);

                // send creation request to the server
                request(app)
                    .post('/api/v1/users/')
                    .send(cln)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        res.should.have.property('status', 400);
                        res.should.have.property('body');
                        res.body.should.have.property('name', 'ValidationError');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property('email');
                        res.body.errors.email.should.have.property('path', 'email');

                        done();
                    });
            });
        });

        var requiredFields = [
            {
                article: 'a',
                name: 'firstName',
                invalid: undefined
            },
            {
                article: 'a',
                name: 'lastName',
                invalid: undefined
            },
            {
                article: 'an',
                name: 'email',
                invalid: undefined
            },
            {
                article: 'a',
                name: 'gender',
                invalid: 'somethingInvalid'
            }
        ];

        requiredFields.forEach(function(field) {
            it('should fail creating a user without ' + field.article + ' ' + field.name, function (done) {
                var _user = _.clone(user1);
                _user[field.name] = field.invalid;

                // send creation request to the server
                request(app)
                    .post('/api/v1/users/')
                    .send(_user)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 400);
                        res.should.have.property('body');
                        res.body.should.have.property('name', 'ValidationError');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property(field.name);
                        res.body.errors[field.name].should.have.property('path', field.name);

                        done();
                    });
            });
        });
    });

    describe('Read', function() {
        beforeEach(function(done) {
            user1 = {
                firstName: 'first_' + helper.getRandomString(),
                lastName: 'last_' + helper.getRandomString(),
                email: 'test' + helper.getRandomString() + '@test.com',
                organisation: [organisation1._id + ''],
                gender: 'male'
            };

            new User(user1).save(function(err, usr) {
                user1._id = usr._id;
                should.not.exist(err);
                done();
            });
        });

        after(function (done) {
            mongoose.connection.collections.users.drop(function() {
                done();
            });
        });

        it('should get users', function(done) {
            // fetch users
            request(app)
                .get('/api/v1/users/')
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');
                    res.body.length.should.equal(1);
                    res.body[0]._id.should.equal(user1._id + '');
                    done();
            });
        });

        it('should get a specific user', function (done) {

            // fetch the user
            request(app)
                .get('/api/v1/users/' + user1._id)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body').and.type('object');
                    res.body._id.should.equal(user1._id + '');

                    done();
                });
        });
    });

    describe('Update', function() {
        beforeEach(function(done) {
            user1 = {
                firstName: 'first_' + helper.getRandomString(),
                lastName: 'last_' + helper.getRandomString(),
                email: 'test' + helper.getRandomString() + '@test.com',
                organisation: [organisation1._id + ''],
                gender: 'male'
            };

            done();
        });

        it('should correctly update an existing user', function (done) {
            // create a user to update
            var _user = new User(user1);
            _user.save(function(err) {
                should.not.exist(err);

                // update a field
                user1.firstName = 'myNewFirstname';
                user1.lastName = 'myNewLastname';
                user1.email = 'newemail' + helper.getRandomString() + '@test.com';
                user1.gender = 'female';

                // send the update to the server
                request(app)
                    .put('/api/v1/users/' + _user._id)
                    .send({
                        firstName: user1.firstName,
                        lastName: user1.lastName,
                        email: user1.email,
                        gender: user1.gender
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body.should.have.property('_id', _user._id + '');
                        res.body.should.have.property('firstName', user1.firstName);
                        res.body.should.have.property('lastName', user1.lastName);
                        res.body.should.have.property('email', user1.email);
                        res.body.should.have.property('gender', user1.gender);

                        // verify the database
                        User.find({
                            _id: _user._id
                        }, function(err, users) {
                            users.should.have.length(1);
                            users[0].should.have.property('firstName', user1.firstName);

                            users[0].remove();
                            done();
                        });
                    });
            });
        });

        it('should accept more than 1 organisation on a user', function (done) {
            // create a user to update
            var _user = new User(user1);
            _user.save(function(err) {
                should.not.exist(err);

                // update a field
                user1.organisation = [organisation1._id + '', organisation2._id + ''];

                // send the update to the server
                request(app)
                    .put('/api/v1/users/' + _user._id)
                    .send({
                        organisation: user1.organisation
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body.should.have.property('_id', _user._id + '');
                        res.body.should.have.property('organisation');
                        res.body.organisation.length.should.equal(2);

                        // verify the database
                        User.find({
                            _id: _user._id
                        }, function(err, users) {
                            users.should.have.length(1);
                            users[0].should.have.property('organisation');
                            users[0].organisation.length.should.equal(2);

                            users[0].remove();
                            done();
                        });
                    });
            });
        });
    });

    describe('Delete', function() {
        before(function(done) {
            user1 = {
                firstName: 'first_' + helper.getRandomString(),
                lastName: 'last_' + helper.getRandomString(),
                email: 'test' + helper.getRandomString() + '@test.com',
                organisation: [organisation1._id + ''],
                gender: 'male'
            };
            done();
        });

        it('should remove an existing user without problem', function (done) {
            // create a user to delete
            var _user = new User(user1);
            _user.save(function(err) {
                should.not.exist(err);

                // send deletion request to the server
                request(app)
                    .delete('/api/v1/users/' + _user._id)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify result
                        res.should.have.property('status', 204);

                        // verify database
                        User.findById(_user._id, function(err, user) {
                            should.not.exist(err);
                            should.exist(user, 'User doesn\'t exist');
                            user.should.have.property('isDeleted', true);
                            done();
                        });
                    });
            });
        });
    });

    describe('User History', function () {
        var _user;

        before(function (done) {
            user1 = {
                firstName: 'first_' + helper.getRandomString(),
                lastName: 'last_' + helper.getRandomString(),
                email: 'test' + helper.getRandomString() + '@test.com',
                organisation: [organisation1._id + ''],
                gender: 'male'
            };

            _user = new User(user1);
            _user.save(function(err) {
                done();
            });
        });

        it('should be empty for a new user', function (done) {
            request(app)
                .get('/api/v1/users/' + _user._id + '/history')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body', []);
                    done();
                });
        });

        it('should be have a change for each modification to the user', function (done) {
            var old = JSON.parse(JSON.stringify(user1));
            _user.set({
                firstName: 'newFirstname',
                gender: 'x'
            });

            _user.save(function(err, org) {
                request(app)
                    .get('/api/v1/users/' + _user._id + '/history?populate=_refId')
                    .end(function(err, res) {
                        if(err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body');
                        res.body.length.should.equal(1);
                        _.each(props, function (prop) {
                            if(old[prop]) {
                                res.body[0].should.have.property(prop, old[prop]);
                            }
                        });
                        res.body[0].should.have.property('_action', 'save');
                        done();
                    });
            });
        });
    });

    describe('Organisation Add', function() {
        beforeEach(function(done) {
            user1 = {
                firstName: 'first_' + helper.getRandomString(),
                lastName: 'last_' + helper.getRandomString(),
                email: 'test' + helper.getRandomString() + '@test.com',
                organisation: [organisation1._id + ''],
                gender: 'male'
            };

            done();
        });

        it('should correctly add an organisation to an existing user', function (done) {
            // create a user to update
            var _user = new User(user1);
            _user.save(function(err) {
                should.not.exist(err);

                // update a field
                user1.organisation.push(organisation2._id + '');

                // send the update to the server
                request(app)
                    .post('/api/v1/users/' + _user._id + '/organisations')
                    .send({
                        organisation: organisation2._id
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body.should.have.property('_id', _user._id + '');
                        res.body.organisation.length.should.equal(2);

                        // verify the database
                        User.find({
                            _id: _user._id
                        }, function(err, users) {
                            users.should.have.length(1);
                            users[0].should.have.property('organisation');
                            users[0].organisation.length.should.equal(2);

                            users[0].remove(function() {
                                done();
                            });
                        });
                    });
            });
        });
    });

    describe('Organisation Delete', function() {
        before(function(done) {
            user1 = {
                firstName: 'first_' + helper.getRandomString(),
                lastName: 'last_' + helper.getRandomString(),
                email: 'test' + helper.getRandomString() + '@test.com',
                organisation: [organisation1._id + '', organisation2._id],
                gender: 'male'
            };

            done();
        });

        it('should remove an organisation from the user', function (done) {
            // create a user with 1 or more organisations
            var _user = new User(user1);
            _user.save(function(err, user) {
                should.not.exist(err);
                var currentUserId = _user._id;

                // send deletion request to the server
                request(app)
                    .delete('/api/v1/users/' + currentUserId + '/organisations/' + organisation2._id)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        // verify result
                        res.should.have.property('status', 200);
                        res.should.have.property('body');
                        res.body.organisation.length.should.equal(1);
                        res.body.organisation[0].should.equal(organisation1._id + '');

                        // verify database
                        User.findById(currentUserId, function(err, dbUser) {
                            dbUser.organisation.should.have.length(1);
                            // cleanup
                            dbUser.remove(function() {
                                done();
                            });
                        });
                    });
            });
        });
    });

});
