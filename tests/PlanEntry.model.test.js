'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    PlanEntry = mongoose.model('PlanEntry'),
    helper = require('./../utils/test-helper'),
    appConfig = require('./../app/config/config');

/**
 * Globals
 */
var planEntry1,
    user = {
        _id: '543555decee89f6e1f78db59'
    },
    project = {
        _id: '543555decee89f6e1f78db58'
    },
    tomorrow1 = moment().add(1, 'd').startOf('day').hours(8).toISOString(),
    tomorrow2 = moment().add(1, 'd').startOf('day').hours(12).toISOString(),
    nextweek1 = moment().add(7, 'd').startOf('day').hours(8).toISOString(),
    nextweek2 = moment().add(7, 'd').startOf('day').hours(12).toISOString(),
    yesterday1 = moment().subtract(1, 'd').startOf('day').hours(8).toISOString(),
    yesterday2 = moment().subtract(1, 'd').startOf('day').hours(12).toISOString();

/**
 * Test Suites
 */
describe('<Unit Test> PlanEntry model:', function() {

    before(function(done) {
        planEntry1 = {
            location: 'kontich',
            start: tomorrow1,
            end: tomorrow2,
            user: user._id,
            project: project._id
        };

        done();
    });

    describe('Config', function() {
        it('should exist', function(done) {
            should.exist(appConfig);
            done();
        });
        it('should contain an enum for locations', function(done) {
            should.exist(appConfig.enums);
            should.exist(appConfig.enums.locations);
            should(appConfig.enums.locations).be.an.instanceOf(Array);
            done();
        });
    });

    describe('Method Save', function() {

        it('should be able to save without problems', function(done) {
            var _planEntry = new PlanEntry(planEntry1);
            _planEntry.save(function(err) {
                should.not.exist(err);
                _planEntry.remove(function() {
                    done();
                });
            });
        });

        it('should be able to create planEntry and save planEntry for updates without problems', function(done) {
            var _planEntry = new PlanEntry(planEntry1);
            _planEntry.save(function(err) {
                should.not.exist(err);
                _planEntry.detailDescription = 'New description';
                _planEntry.save(function(err) {
                    should.not.exist(err);
                    _planEntry.detailDescription.should.equal('New description');
                    _planEntry.remove(function() {
                        done();
                    });
                });
            });
        });

        it('should show an error when try to save without start time', function(done) {
            var _planEntry = new PlanEntry(planEntry1);
            _planEntry.start = '';
            return _planEntry.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when try to save without end time', function(done) {
            var _planEntry = new PlanEntry(planEntry1);
            _planEntry.end = '';
            return _planEntry.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when you supply an end time before the begin time', function(done) {
            var _planEntry = new PlanEntry(planEntry1);
            _planEntry.end = planEntry1.start;
            _planEntry.start = planEntry1.end;
            return _planEntry.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when try to save without project', function(done) {
            var _planEntry = new PlanEntry(planEntry1);
            _planEntry.project = '';
            return _planEntry.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when try to save without location', function(done) {
            var _planEntry = new PlanEntry(planEntry1);
            _planEntry.location = '';
            return _planEntry.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when try to save with an invalid location', function(done) {
            var _planEntry = new PlanEntry(planEntry1);
            _planEntry.location = 'somewhere else';
            return _planEntry.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when try to save without user`', function(done) {
            var _planEntry = new PlanEntry(planEntry1);
            _planEntry.user = '';
            return _planEntry.save(function(err) {
                should.exist(err);
                done();
            });
        });
    });

    after(function(done) {

        /** Clean up planEntry objects
         * un-necessary as they are cleaned up in each test but kept here
         * for educational purposes
         *
         *  var _planEntry1 = new PlanEntry(planEntry1);
         *
         *  _planEntry1.remove();
         */

        done();
    });
});
