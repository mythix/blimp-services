'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    Project = mongoose.model('Project'),
    helper = require('./../utils/test-helper');

/**
 * Globals
 */
var project1, project2,
    organisation = {
        _id: '543555decee89f6e1f78db59'
    };

/**
 * Test Suites
 */
describe('<Unit Test> Project model:', function() {

    before(function(done) {
        project1 = {
            name: 'project_' + helper.getRandomString(),
            description: 'desc_' + helper.getRandomString(),
            organisation: organisation._id
        };

        project2 = {
            name: 'project_' + helper.getRandomString(),
            description: 'desc_' + helper.getRandomString(),
            organisation: organisation._id
        };

        done();
    });

    describe('Method Save', function() {
        it('should begin without the test project', function(done) {
            Project.find({
                name: project1.name
            }, function(err, projects) {
                    projects.should.have.length(0);

                    Project.find({
                        name: project2.name
                    }, function(err, projects) {
                            projects.should.have.length(0);
                            done();
                        });
                });
        });

        it('should be able to save without problems', function(done) {
            var _project = new Project(project1);
            _project.save(function(err) {
                should.not.exist(err);
                _project.remove(function() {
                    done();
                });
            });
        });

        it('should be able to create project and save project for updates without problems', function(done) {
            var _project = new Project(project1);
            var newOrgName = 'newOrgName' + helper.getRandomString();
            _project.save(function(err) {
                should.not.exist(err);
                _project.name = newOrgName;
                _project.save(function(err) {
                    should.not.exist(err);
                    _project.name.should.equal(newOrgName);
                    _project.remove(function() {
                        done();
                    });
                });
            });
        });

        it('should fail to save an existing project with the same values', function(done) {
            var _project1 = new Project(project1);
            _project1.save();

            var _project2 = new Project(project1);

            return _project2.save(function(err) {
                should.exist(err);
                _project1.remove(function() {
                    if (!err) {
                        _project2.remove(function() {
                            done();
                        });
                    }
                    done();
                });
            });
        });

        it('should show an error when try to save without name', function(done) {
            var _project = new Project(project1);
            _project.name = '';
            return _project.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when try to save without organisation', function(done) {
            var _project = new Project(project1);
            _project.organisation = '';
            return _project.save(function(err) {
                should.exist(err);
                done();
            });
        });
    });

    after(function(done) {

        /** Clean up project objects
         * un-necessary as they are cleaned up in each test but kept here
         * for educational purposes
         *
         *  var _project1 = new Project(project1);
         *  var _project2 = new Project(project2);
         *
         *  _project1.remove();
         *  _project2.remove();
         */

        done();
    });
});
