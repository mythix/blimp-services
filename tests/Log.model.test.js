'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    Log = mongoose.model('Log'),
    helper = require('./../utils/test-helper');

/**
 * Globals
 */
var log1, log2;

/**
 * Test Suites
 */
describe('<Unit Test> Log model:', function() {

    before(function(done) {
        log1 = {
            category: ['system', 'client'],
            message: 'desc_' + helper.getRandomString()
        };

        log2 = {
            category: ['system'],
            message: 'desc_' + helper.getRandomString()
        };

        done();
    });

    describe('Method Save', function() {
        it('should begin without the test log', function(done) {
            Log.find({
                message: log1.message
            }, function(err, logs) {
                    logs.should.have.length(0);

                    Log.find({
                        message: log2.message
                    }, function(err, logs) {
                            logs.should.have.length(0);
                            done();
                        });
                });
        });

        it('should be able to save without problems', function(done) {
            var _log = new Log(log1);
            _log.save(function(err) {
                should.not.exist(err);
                _log.remove(function() {
                    done();
                });
            });
        });

        it('should be able to create log and save log for updates without problems', function(done) {
            var _log = new Log(log1);
            var newMessage = 'newMessage_' + helper.getRandomString();
            _log.save(function(err) {
                should.not.exist(err);
                _log.message = newMessage;
                _log.save(function(err) {
                    should.not.exist(err);
                    _log.message.should.equal(newMessage);
                    _log.remove(function() {
                        done();
                    });
                });
            });
        });

        it('should show an error when try to save without message', function(done) {
            var _log = new Log(log1);
            _log.message = '';
            return _log.save(function(err) {
                should.exist(err);
                done();
            });
        });
    });
});
