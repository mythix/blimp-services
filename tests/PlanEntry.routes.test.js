'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    request = require('supertest'),
    mongoose = require('mongoose'),
    _ = require('lodash'),
    moment = require('moment'),
    config = require('./../app/config/config'),
    helper = require('./../utils/test-helper'),
    app = require('./../app/app'),
    PlanEntry = mongoose.model('PlanEntry'),
    Project = mongoose.model('Project'),
    User = mongoose.model('User'),
    appConfig = require('./../app/config/config');

/**
 * Globals
 */
var planentry1,
    user1,
    project1,
    tomorrow1 = moment().add(1, 'd').startOf('day').hours(8).toISOString(),
    tomorrow2 = moment().add(1, 'd').startOf('day').hours(12).toISOString(),
    nextweek1 = moment().add(7, 'd').startOf('day').hours(8).toISOString(),
    nextweek2 = moment().add(7, 'd').startOf('day').hours(12).toISOString(),
    yesterday1 = moment().subtract(1, 'd').startOf('day').hours(8).toISOString(),
    yesterday2 = moment().subtract(1, 'd').startOf('day').hours(12).toISOString(),
    props = Object.keys(PlanEntry.schema.paths);

props = _.filter(props, function (prop) {
    return prop[0] !== '_';
});

/**
 * Test Suites
 */
describe('<Integration Test> Plan Entry routes', function() {

    // create a project and a user to use in our entries
    before(function(done) {
        project1 = new Project({
            name: 'org_' + helper.getRandomString()
        });
        user1 = new User({
            email: 'usr_' + helper.getRandomString() + '@test.com',
            firstName: 'usr_' + helper.getRandomString(),
            lastName: 'usr_' + helper.getRandomString(),
            project: project1
        });

        mongoose.connection.collections.planentries.drop(function() {
            mongoose.connection.collections.planentry_versions.drop(function() {
                done();
            });
        });
    });

    describe('Start', function() {
        beforeEach(function(done) {
            planentry1 = {
                detailDescription: 'planentry_' + helper.getRandomString(),
                project: project1._id + '',
                user: user1._id + '',
                start: tomorrow1,
                end: tomorrow2
            };
            done();
        });

        it('should begin without the testing entry', function(done) {
            // find entry one to test if it really isn't there
            PlanEntry.find({
                detailDescription: planentry1.detailDescription
            }, function(err, entries) {
                entries.should.have.length(0);
                done();
            });
        });
    });

    describe('Schema', function () {

        it('should return the user\'s schema', function (done) {
            request(app)
                .get('/api/v1/plan-entries/schema')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');
                    res.body.should.have.property('resource', 'PlanEntry');
                    res.body.should.have.property('fields');
                    res.body.fields.should.have.property('_id');
                    _.each(props, function (prop) {
                        res.body.fields.should.have.property(prop);
                    });
                });
            done();
        });
    });

    describe('Create', function() {
        beforeEach(function(done) {
            planentry1 = {
                detailDescription: 'planentry_' + helper.getRandomString(),
                project: project1._id + '',
                user: user1._id + '',
                start: tomorrow1,
                end: tomorrow2,
                location: 'kontich'
            };
            done();
        });

        it('should add a new planEntry', function(done) {
            // send creation request to the server
            request(app)
                .post('/api/v1/plan-entries/')
                .send(planentry1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify result
                    res.should.have.property('status', 201);
                    res.should.have.property('body');
                    res.body.should.be.type('object');
                    res.body.detailDescription.should.equal(planentry1.detailDescription);
                    res.body.start.should.equal(planentry1.start);
                    res.body.end.should.equal(planentry1.end);
                    res.body.user.should.equal(planentry1.user);
                    res.body.project.should.equal(planentry1.project);
                    res.body.location.should.equal(planentry1.location);


                    // verify database
                    PlanEntry.find({
                        _id: res.body._id
                    }, function(err, entries) {
                        entries.should.have.length(1);
                        done();
                    });
                });
        });

        it('should fail creating a planEntry with an invalid location', function (done) {
            planentry1.location = 'invalid';

            // send creation request to the server
            request(app)
                .post('/api/v1/plan-entries/')
                .send(planentry1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('location');
                    res.body.errors.location.should.have.property('path', 'location');
                    appConfig.enums.locations.should.not.containEql(planentry1.location);
                    done();
                });
        });

        it('should fail creating a planEntry without a project', function (done) {
            planentry1.project = undefined;

            // send creation request to the server
            request(app)
                .post('/api/v1/plan-entries/')
                .send(planentry1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('project');
                    res.body.errors.project.should.have.property('path', 'project');

                    done();
                });
        });

        it('should fail creating a planEntry without a user', function (done) {
            planentry1.user = undefined;

            // send creation request to the server
            request(app)
                .post('/api/v1/plan-entries/')
                .send(planentry1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('user');
                    res.body.errors.user.should.have.property('path', 'user');

                    done();
                });
        });

        it('should fail creating a planEntry without a start date', function (done) {
            planentry1.start = undefined;

            // send creation request to the server
            request(app)
                .post('/api/v1/plan-entries/')
                .send(planentry1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('start');
                    res.body.errors.start.should.have.property('path', 'start');

                    done();
                });
        });

        it('should fail creating a planEntry without an end date', function (done) {
            planentry1.end = undefined;

            // send creation request to the server
            request(app)
                .post('/api/v1/plan-entries/')
                .send(planentry1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('end');
                    res.body.errors.end.should.have.property('path', 'end');

                    done();
                });
        });

        it.skip('should fail creating a planEntry in the past', function (done) {
            planentry1.start = yesterday1;
            planentry1.end = yesterday2;

            // send creation request to the server
            request(app)
                .post('/api/v1/plan-entries/')
                .send(planentry1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 400);
                    res.should.have.property('body');
                    res.body.should.have.property('name', 'ValidationError');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('start');
                    res.body.errors.start.should.have.property('path', 'start');

                    done();
                });
        });

        it('should add a new planEntry even when creating a still running planEntry', function (done) {
            planentry1.start = yesterday1;
            planentry1.end = tomorrow2;

            // send creation request to the server
            request(app)
                .post('/api/v1/plan-entries/')
                .send(planentry1)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.should.have.property('status', 201);
                    res.should.have.property('body');
                    res.body.should.be.type('object');
                    res.body.detailDescription.should.equal(planentry1.detailDescription);
                    res.body.start.should.equal(planentry1.start);
                    res.body.end.should.equal(planentry1.end);
                    res.body.user.should.equal(planentry1.user);
                    res.body.project.should.equal(planentry1.project);
                    res.body.location.should.equal(planentry1.location);

                    done();
                });
        });
    });

    describe('Read', function() {
        beforeEach(function(done) {
            planentry1 = {
                detailDescription: 'planentry_' + helper.getRandomString(),
                project: project1._id + '',
                user: user1._id + '',
                start: tomorrow1,
                end: tomorrow2
            };
            done();
        });

        it('should get entries', function(done) {
            // fetch entries
            request(app)
                .get('/api/v1/plan-entries/')
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body');

                    done();
            });
        });

        it('should get a specific planEntry', function (done) {
            // create a planEntry to fetch
            var _planEntry = new PlanEntry(planentry1);
            _planEntry.save(function(err) {
                should.not.exist(err);

                // fetch the planEntry
                request(app)
                    .get('/api/v1/plan-entries/' + _planEntry._id)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body._id.should.equal(_planEntry._id + '');

                        done();
                    });
            });
        });
    });

    describe('Update', function() {
        beforeEach(function(done) {
            planentry1 = {
                detailDescription: 'planentry_' + helper.getRandomString(),
                project: project1._id + '',
                user: user1._id + '',
                start: tomorrow1,
                end: tomorrow2
            };
            done();
        });

        it('should correctly update an existing planEntry in the future', function (done) {
            // create a planEntry to update
            var _planEntry = new PlanEntry(planentry1);
            _planEntry.save(function(err) {
                should.not.exist(err);

                // update a field
                planentry1.detailDescription = 'Shiny description for our planEntry';
                planentry1.start = nextweek1;
                planentry1.end = nextweek2;
                planentry1.location = 'external';

                // send the update to the server
                request(app)
                    .put('/api/v1/plan-entries/' + _planEntry._id)
                    .send({
                        detailDescription: planentry1.detailDescription,
                        end: planentry1.end,
                        start: planentry1.start,
                        location: planentry1.location
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body.should.have.property('_id', _planEntry._id + '');
                        res.body.should.have.property('detailDescription', planentry1.detailDescription);
                        res.body.should.have.property('start', planentry1.start);
                        res.body.should.have.property('end', planentry1.end);
                        res.body.should.have.property('location', planentry1.location);

                        // verify the database
                        PlanEntry.find({
                            _id: _planEntry._id
                        }, function(err, entries) {
                            entries.should.have.length(1);
                            entries[0].should.have.property('detailDescription', planentry1.detailDescription);
                            entries[0].should.have.property('start', new Date(planentry1.start));
                            entries[0].should.have.property('end', new Date(planentry1.end));
                            entries[0].should.have.property('location', planentry1.location);
                            done();
                        });
                    });
            });
        });

        it('should correctly update an existing planEntry to currently running', function (done) {
            // create a planEntry to update
            var _planEntry = new PlanEntry(planentry1);
            _planEntry.save(function(err) {
                should.not.exist(err);

                // update a field
                planentry1.detailDescription = 'Shiny description for our planEntry';
                planentry1.start = yesterday1;
                planentry1.end = nextweek2;
                planentry1.location = 'external';

                // send the update to the server
                request(app)
                    .put('/api/v1/plan-entries/' + _planEntry._id)
                    .send({
                        detailDescription: planentry1.detailDescription,
                        end: planentry1.end,
                        start: planentry1.start,
                        location: planentry1.location
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body').and.type('object');
                        res.body.should.have.property('_id', _planEntry._id + '');
                        res.body.should.have.property('detailDescription', planentry1.detailDescription);
                        res.body.should.have.property('start', planentry1.start);
                        res.body.should.have.property('end', planentry1.end);
                        res.body.should.have.property('location', planentry1.location);

                        // verify the database
                        PlanEntry.find({
                            _id: _planEntry._id
                        }, function(err, entries) {
                            entries.should.have.length(1);
                            entries[0].should.have.property('detailDescription', planentry1.detailDescription);
                            entries[0].should.have.property('start', new Date(planentry1.start));
                            entries[0].should.have.property('end', new Date(planentry1.end));
                            entries[0].should.have.property('location', planentry1.location);
                            done();
                        });
                    });
            });
        });

        it.skip('should fail updating an existing planEntry to a date fully in the past', function (done) {
            // create a planEntry to update
            var _planEntry = new PlanEntry(planentry1);
            _planEntry.save(function(err) {
                should.not.exist(err);

                var _original = _.clone(planentry1);

                // update a field
                planentry1.detailDescription = 'Shiny description for our planEntry';
                planentry1.start = yesterday1;
                planentry1.end = yesterday2;
                planentry1.location = 'external';

                // send the update to the server
                request(app)
                    .put('/api/v1/plan-entries/' + _planEntry._id)
                    .send({
                        detailDescription: planentry1.detailDescription,
                        end: planentry1.end,
                        start: planentry1.start,
                        location: planentry1.location
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 400);
                        res.should.have.property('body');
                        res.body.should.have.property('name', 'ValidationError');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property('start');
                        res.body.errors.start.should.have.property('path', 'start');

                        // verify the database
                        PlanEntry.find({
                            _id: _planEntry._id
                        }, function(err, entries) {
                            entries.should.have.length(1);
                            entries[0].should.have.property('detailDescription', _original.detailDescription);
                            entries[0].should.have.property('start', new Date(_original.start));
                            entries[0].should.have.property('end', new Date(_original.end));
                            entries[0].should.have.property('location', _original.location);
                            entries[0].should.have.property('user', _original.user);
                            entries[0].should.have.property('project', _original.project);
                            done();
                        });
                    });
            });
        });

        it.skip('should fail updating an existing planEntry that was already in the past', function (done) {
            // create a planEntry to update
            planentry1.start = yesterday1;
            planentry1.end = yesterday2;

            var _planEntry = new PlanEntry(planentry1);
            _planEntry.save(function(err) {
                should.not.exist(err);

                var _original = _.clone(planentry1);

                // update a field
                planentry1.detailDescription = 'Shiny description for our planEntry';
                planentry1.start = nextweek1;
                planentry1.end = nextweek2;
                planentry1.location = 'external';

                // send the update to the server
                request(app)
                    .put('/api/v1/plan-entries/' + _planEntry._id)
                    .send({
                        detailDescription: planentry1.detailDescription,
                        end: planentry1.end,
                        start: planentry1.start,
                        location: planentry1.location
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 400);
                        res.should.have.property('body');
                        res.body.should.have.property('name', 'ValidationError');
                        res.body.should.have.property('errors');
                        res.body.errors.should.have.property('start');
                        res.body.errors.start.should.have.property('path', 'start');

                        // verify the database
                        PlanEntry.find({
                            _id: _planEntry._id
                        }, function(err, entries) {
                            entries.should.have.length(1);
                            entries[0].should.have.property('detailDescription', _original.detailDescription);
                            entries[0].should.have.property('start', new Date(_original.start));
                            entries[0].should.have.property('end', new Date(_original.end));
                            entries[0].should.have.property('location', _original.location);
                            entries[0].should.have.property('user', _original.user);
                            entries[0].should.have.property('project', _original.project);
                            done();
                        });
                    });
            });
        });
    });

    describe('Delete', function() {
        beforeEach(function(done) {
            planentry1 = {
                detailDescription: 'planentry_' + helper.getRandomString(),
                project: project1,
                user: user1,
                start: tomorrow1,
                end: tomorrow2,
                location: 'ghent'
            };
            done();
        });

        it('should remove an existing planEntry without problem', function (done) {
            // create a planEntry to delete
            var _planEntry = new PlanEntry(planentry1);
            _planEntry.save(function(err) {
                should.not.exist(err);

                // send deletion request to the server
                request(app)
                    .delete('/api/v1/plan-entries/' + _planEntry._id)
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }

                        // verify result
                        res.should.have.property('status', 204);

                        // verify database
                        PlanEntry.findById(_planEntry._id, function(err, entry) {
                            should.not.exist(err);
                            should.exist(entry, 'Entry doesn\'t exist');
                            entry.should.have.property('isDeleted', true);
                            done();
                        });
                    });
            });
        });
    });

    describe('PlanEntry History', function () {
        var _planEntry;

        before(function (done) {
            planentry1 = {
                detailDescription: 'planentry_' + helper.getRandomString(),
                project: project1,
                user: user1,
                start: tomorrow1,
                end: tomorrow2,
                location: 'ghent'
            };

            _planEntry = new PlanEntry(planentry1);
            _planEntry.save(function(err) {
                done();
            });
        });

        it('should be empty for a new planEntry', function (done) {
            request(app)
                .get('/api/v1/plan-entries/' + _planEntry._id + '/history')
                .end(function(err, res) {
                    if(err) {
                        throw err;
                    }

                    // verify the result
                    res.should.have.property('status', 200);
                    res.should.have.property('body', []);
                    done();
                });
        });

        it('should be have a change for each modification to the planEntry', function (done) {
            var old = JSON.parse(JSON.stringify(_planEntry));
            _planEntry.set({
                detailDescription: 'new description',
                start: nextweek1,
                end: nextweek2
            });

            _planEntry.save(function(err, org) {
                request(app)
                    .get('/api/v1/plan-entries/' + _planEntry._id + '/history')
                    .end(function(err, res) {
                        if(err) {
                            throw err;
                        }

                        // verify the result
                        res.should.have.property('status', 200);
                        res.should.have.property('body');
                        res.body.length.should.equal(1);
                        _.each(props, function (prop) {
                           if (old[prop]) {
                                res.body[0].should.have.property(prop, old[prop]);
                            }
                        });
                        res.body[0].should.have.property('_action', 'save');
                        done();
                    });
            });
        });
    });
});
