'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    helper = require('./../utils/test-helper'),
    appConfig = require('./../app/config/config');

/**
 * Globals
 */
var user1, user2,
    organisation = {
        _id: '543555decee89f6e1f78db59'
    },
    organisation2 = {
        _id: '543555decee89f6e1f78db14'
    };

/**
 * Test Suites
 */
describe('<Unit Test> User model:', function() {

    before(function(done) {
        user1 = {
            firstName: 'first_' + helper.getRandomString(),
            lastName: 'last_' + helper.getRandomString(),
            email: 'test' + helper.getRandomString() + '@test.com',
            organisation: organisation._id,
            gender: 'male'
        };

        user2 = {
            firstName: 'first_' + helper.getRandomString(),
            lastName: 'last_' + helper.getRandomString(),
            email: 'test' + helper.getRandomString() + '@test.com',
            organisation: organisation._id,
            gender: 'female'
        };

        done();
    });

    describe('Config', function() {
        it('should exist', function(done) {
            should.exist(appConfig);
            done();
        });
        it('should contain an enum for genders', function(done) {
            should.exist(appConfig.enums);
            should.exist(appConfig.enums.genders);
            should(appConfig.enums.genders).be.an.instanceOf(Array);
            done();
        });
    });

    describe('Method Save', function() {
        it('should begin without the test user', function(done) {
            User.find({
                email: user1.email
            }, function(err, users) {
                    users.should.have.length(0);

                    User.find({
                        email: user2.email
                    }, function(err, users) {
                            users.should.have.length(0);
                            done();
                        });
                });
        });

        it('should be able to save without problems', function(done) {
            var _user = new User(user1);
            _user.save(function(err) {
                should.not.exist(err);
                _user.remove(function() {
                    done();
                });
            });
        });

        it('should be able to create user and save user for updates without problems', function(done) {
            var _user = new User(user1);
            _user.save(function(err) {
                should.not.exist(err);
                _user.name = 'Full name2';
                _user.save(function(err) {
                    should.not.exist(err);
                    _user.name.should.equal('Full name2');
                    _user.remove(function() {
                        done();
                    });
                });
            });
        });

        it('should fail to save an existing user with the same values', function(done) {
            var _user1 = new User(user1);
            _user1.save();

            var _user2 = new User(user1);

            return _user2.save(function(err) {
                should.exist(err);
                _user1.remove(function() {
                    if (!err) {
                        _user2.remove(function() {
                            done();
                        });
                    }
                    done();
                });
            });
        });

        it('should accept just 1 organisation on a user', function(done) {
            var _user = new User(user2);
            _user.organisation = organisation2._id;

            _user.save(function(err) {
                should.not.exist(err);
                _user.organisation.length.should.equal(1);
                _user.remove(function() {
                    done();
                });
            });
        });

        it('should also accept more than 1 organisation on a user', function(done) {
            var _user = new User(user2);
            _user.organisation = [organisation._id, organisation2._id];

            _user.save(function(err) {
                should.not.exist(err);
                _user.organisation.length.should.equal(2);
                _user.remove(function() {
                    done();
                });
            });
        });

        it('should show an error when try to save without firstName', function(done) {
            var _user = new User(user1);
            _user.firstName = '';
            return _user.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when try to save without lastName', function(done) {
            var _user = new User(user1);
            _user.lastName = '';
            return _user.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when try to save without email', function(done) {
            var _user = new User(user1);
            _user.email = '';
            return _user.save(function(err) {
                should.exist(err);
                done();
            });
        });

        it('should show an error when try to save without organisation', function(done) {
            var _user = new User(user1);
            _user.organisation = '';
            return _user.save(function(err) {
                should.exist(err);
                done();
            });
        });
    });

    // source: http://en.wikipedia.org/wiki/Email_address
    describe('Test Email Validations', function() {
        it('Shouldnt allow invalid emails #1', function(done) {
            var _user = new User(user1);
            _user.email = 'Abc.example.com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.exist(err);
                        done();
                    });
                } else {
                    should.exist(err);
                    done();
                }
            });
        });

        it('Shouldnt allow invalid emails #2', function(done) {
            var _user = new User(user1);
            _user.email = 'A@b@c@example.com';
            _user.save(function(err) {
                if (err) {
                    should.exist(err);
                    done();
                } else {
                    _user.remove(function(err2) {
                        should.exist(err);
                        done();
                    });
                }
            });
        });

        it('Shouldnt allow invalid emails #3', function(done) {
            var _user = new User(user1);
            _user.email = 'a"b(c)d,e:f;g<h>i[j\\k]l@example.com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.exist(err);
                        done();
                    });
                } else {
                    should.exist(err);
                    done();
                }
            });
        });

        it('Shouldnt allow invalid emails #4', function(done) {
            var _user = new User(user1);
            _user.email = 'just"not"right@example.com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.exist(err);
                        done();
                    });
                } else {
                    should.exist(err);
                    done();
                }
            });
        });

        it('Shouldnt allow invalid emails #5', function(done) {
            var _user = new User(user1);
            _user.email = 'this is"not\\allowed@example.com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.exist(err);
                        done();
                    });
                } else {
                    should.exist(err);
                    done();
                }
            });
        });

        it('Shouldnt allow invalid emails #6', function(done) {
            var _user = new User(user1);
            _user.email = 'this\\ still"not\\allowed@example.com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.exist(err);
                        done();
                    });
                } else {
                    should.exist(err);
                    done();
                }
            });
        });

        it('Shouldnt allow invalid emails #7', function(done) {
            var _user = new User(user1);
            _user.email = 'john..doe@example.com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.exist(err);
                        done();
                    });
                } else {
                    should.exist(err);
                    done();
                }
            });
        });

        it('Shouldnt allow invalid emails #8', function(done) {
            var _user = new User(user1);
            _user.email = 'john.doe@example..com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.exist(err);
                        done();
                    });
                } else {
                    should.exist(err);
                    done();
                }
            });
        });

        it('Should save with valid email #1', function(done) {
            var _user = new User(user1);
            _user.email = 'john.doe@example.com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.not.exist(err);
                        done();
                    });
                } else {
                    should.not.exist(err);
                    done();
                }
            });
        });

        it('Should save with valid email #2', function(done) {
            var _user = new User(user1);
            _user.email = 'disposable.style.email.with+symbol@example.com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.not.exist(err);
                        done();
                    });
                } else {
                    should.not.exist(err);
                    done();
                }
            });
        });

        it('Should save with valid email #3', function(done) {
            var _user = new User(user1);
            _user.email = 'other.email-with-dash@example.com';
            _user.save(function(err) {
                if (!err) {
                    _user.remove(function() {
                        should.not.exist(err);
                        done();
                    });
                } else {
                    should.not.exist(err);
                    done();
                }
            });
        });

    });

    after(function(done) {

        /** Clean up user objects
         * un-necessary as they are cleaned up in each test but kept here
         * for educational purposes
         *
         *  var _user1 = new User(user1);
         *  var _user2 = new User(user2);
         *
         *  _user1.remove();
         *  _user2.remove();
         */

        done();
    });
});
