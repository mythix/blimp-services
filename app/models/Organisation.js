'use strict';

var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    Schema = mongoose.Schema,
    deleteParanoid = require('mongoose-plugins-delete-paranoid'),
    version = require('mongoose-version2'),
    restful = require('node-restful'),
    config = require('./../config/config');

var OrganisationSchema = new Schema({
    name: {
        type: 'string',
        unique: true,
        index: true,
        required: true
    },
    logo: {
        type: 'string'
    }
});

// Initialize pluggins
OrganisationSchema.plugin(uniqueValidator, {
    message: config.uniqueValidatorMessage
});
OrganisationSchema.plugin(version, {});
OrganisationSchema.plugin(deleteParanoid.deleteParanoid);

module.exports = restful.model('Organisation', OrganisationSchema);
