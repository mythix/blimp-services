'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    deleteParanoid = require('mongoose-plugins-delete-paranoid'),
    version = require('mongoose-version2'),
    restful = require('node-restful'),
    config = require('./../config/config');

var locations = config.enums.locations;
var PlanEntrySchema = new Schema({
    start: {
        type: 'Date',
        required: true
    },
    end: {
        type: 'Date',
        required: true
    },
    detailDescription: {
        type: 'String'
    },
    location: {
        type: 'String',
        required: true,
        enum: locations,
        default: 'kontich'
    },
    project: {
        type: 'ObjectId',
        ref: 'Project',
        required: true
    },
    user: {
        type: 'ObjectId',
        ref: 'User',
        required: true
    }
});

// Validate start date to be before end date
PlanEntrySchema.pre('validate', function(next) {
    // get out if no end date is given, the `required` validator will catch this
    if (!this.end) {
        next();
    }

    // only validate if end date is given
    if (this.start > this.end) {
        next(new Error('End Date must be greater than Start Date'));
    } else {
        next();
    }
});
PlanEntrySchema.plugin(version, {});
PlanEntrySchema.plugin(deleteParanoid.deleteParanoid);

module.exports = restful.model('PlanEntry', PlanEntrySchema);
