'use strict';

var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    Schema = mongoose.Schema,
    deleteParanoid = require('mongoose-plugins-delete-paranoid'),
    version = require('mongoose-version2'),
    restful = require('node-restful'),
    config = require('./../config/config');

var ProjectSchema = new Schema({
    name: {
        type: 'string',
        unique: true,
        index: true,
        required: true
    },
    description: {
        type: 'string'
    },
    organisation: {
        type: 'ObjectId',
        ref: 'Organisation',
        required: true
    }
});

// Initialize uniqueValidator plugin
ProjectSchema.plugin(uniqueValidator, {
    message: config.uniqueValidatorMessage
});
ProjectSchema.plugin(version, {});
ProjectSchema.plugin(deleteParanoid.deleteParanoid);

module.exports = restful.model('Project', ProjectSchema);
