'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    restful = require('node-restful'),
    version = require('mongoose-version2'),
    config = require('./../config/config');

var LogSchema = new Schema({
    category: {
        type: ['string']
    },
    message: {
        type: 'string',
        required: true,
        index: true
    },
    user: {
        type: 'ObjectId',
        ref: 'User',
    }
});

LogSchema.plugin(version, {});

module.exports = restful.model('Log', LogSchema);
