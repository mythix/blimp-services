'use strict';

var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    Schema = mongoose.Schema,
    deleteParanoid = require('mongoose-plugins-delete-paranoid'),
    version = require('mongoose-version2'),
    restful = require('node-restful'),
    config = require('./../config/config');

var genders = config.enums.genders;
var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var UserSchema = new Schema({
    lastName: {
        type: 'string',
        required: true
    },
    firstName: {
        type: 'string',
        required: true
    },
    gender: {
        type: 'string',
        required: true,
        enum: genders,
        default: 'x'
    },
    avatar: {
        type: 'string'
    },
    email: {
        type: 'string',
        required: true,
        index: true,
        unique: true,
        lowercase: true,
        match: [emailRegex, 'Please enter a valid email']
    },
    organisation: [{
        type: 'ObjectId',
        ref: 'Organisation'
    }]
});

// Initialize pluggins
UserSchema.plugin(uniqueValidator, {
    message: config.uniqueValidatorMessage
});
UserSchema.plugin(version, {});
UserSchema.plugin(deleteParanoid.deleteParanoid);

module.exports = restful.model('User', UserSchema);
