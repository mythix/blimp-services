'use strict';

var uberquery = require('uberquery'),
    Model = require('./../models/User'),
    ObjNotFound = require('./../utils/object-not-found'),
    controller = {};

controller.removeOrganisation = function(req, res, next) {
    var organisationToRemove = req.params.organisation;

    Model.update(
        { _id: req.params.id },
        { $pull: { 'organisation': organisationToRemove } }
    )
    .exec(function(err, documentBefore) {
        if(err) {
            uberquery.handlers.respond(res, 500, err);
            uberquery.handlers.send(req, res, next);
            return;
        }

        Model.findById(req.params.id, function (err, document) {
            if(!document) {
                uberquery.handlers.respond(res, 404, new ObjNotFound());
                uberquery.handlers.send(req, res, next);
                return;
            }

            uberquery.handlers.respond(res, 200, document);
            uberquery.handlers.send(req, res, next);
        });
    });
};

controller.addOrganisation = function(req, res, next) {
    if(!req.body.organisation) {
        uberquery.handlers.respond(res, 400, {
            msg: 'parameter mismatch: invalid organisation',
            description: 'The parameter organisation is required but was missing.'
        });
        uberquery.handlers.send(req, res, next);
        return;
    }

    var organisationToAdd = req.body.organisation;

    Model.update(
        { _id: req.params.id },
        { $push: { 'organisation': organisationToAdd } }
    )
    .exec(function(err, result) {
        if(err) {
            uberquery.handlers.respond(res, 500, err);
            uberquery.handlers.send(req, res, next);
            return;
        }
        Model.findById(req.params.id, function (err, document) {
            if(!document) {
                uberquery.handlers.respond(res, 404, new ObjNotFound());
                uberquery.handlers.send(req, res, next);
                return;
            }

            uberquery.handlers.respond(res, 200, document);
            uberquery.handlers.send(req, res, next);
        });
    });
};

module.exports = controller;
