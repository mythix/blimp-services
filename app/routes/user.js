'use strict';

var Model = require('./../models/User'),
    controller = require('./../controllers/user'),
    filterDeleted = require('./../utils/filter-out-deleted-documents'),
    archiveDocument = require('./../utils/archive-document'),
    versionRoutes = require('./../utils/version-routes'),
    path = '/users';

module.exports = function(router) {
    // Default routes
    Model = Model.methods(['get', 'put', 'post']);

    // Add Soft Delete Routes for a project
    Model.route(':id', 'delete', archiveDocument);

    // Add Pre hook for get requests (filter out deleted items if not specifically asked for)
    Model.before('get', filterDeleted);

    // Add a single organisation to a user
    router.post(path + '/:id/organisations', controller.addOrganisation);

    // remove an organisation from a user
    router.delete(path + '/:id/organisations/:organisation', controller.removeOrganisation);

    // add a route to access versions of this model
    versionRoutes(router, Model, path);

    // Remove atomic update/delete
    Model.shouldUseAtomicUpdate = false;

    Model.register(router, path);
};
