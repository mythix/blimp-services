'use strict';

var Model = require('./../models/Project'),
    controller = require('./../controllers/project'),
    filterDeleted = require('./../utils/filter-out-deleted-documents'),
    archiveDocument = require('./../utils/archive-document'),
    versionRoutes = require('./../utils/version-routes'),
    path = '/projects';

module.exports = function(router) {
    // Default routes
    Model = Model.methods(['get', 'put', 'post']);

    // Add Soft Delete Routes for a project
    Model.route(':id', 'delete', archiveDocument);

    // Add Pre hook for get requests (filter out deleted items if not specifically asked for)
    Model.before('get', filterDeleted);

    // add a route to access versions of this model
    versionRoutes(router, Model, path);

    // Remove atomic update/delete
    Model.shouldUseAtomicUpdate = false;

    // Register router on a certain path
    Model.register(router, path);
};
