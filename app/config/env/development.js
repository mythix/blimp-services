'use strict';

module.exports = {
    db: 'mongodb://localhost:27017/blimp-dev',
    app: {
        name: 'Blimp - Development'
    }
};
