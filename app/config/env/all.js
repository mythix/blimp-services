'use strict';

var path = require('path');
var rootPath = path.normalize(__dirname + '/../..');

module.exports = {
    root: rootPath,
    port: process.env.PORT || 5001,
    domain: 'localhost',
    db: process.env.MONGODB_URL,
    logToConsole: true,
    uniqueValidatorMessage: 'Path `{PATH}` must be unique.',
    enums: {
        genders: [
            'male',
            'female',
            'x'
        ],
        locations: [
            'kontich',
            'ghent',
            'home',
            'external'
        ]
    }
};
